const OFF = 0
const WARNING = 1
const ERROR = 2

const RESOLVED_MODULES = [
  '^constants',
  '^pages',
  '^storage',
]

module.exports = {
  parser: 'babel-eslint',
  extends: 'airbnb',
  plugins: ['react', 'jsx-a11y', 'import', 'jest'],
  rules: {
    'arrow-body-style': [OFF],
    'arrow-parens': [ERROR, 'always'],
    'global-require': [OFF],
    'import/no-extraneous-dependencies': [OFF],
    'import/extensions': [ERROR, 'never'],
    'import/no-named-as-default-member': [OFF],
    'import/no-unresolved': [ERROR, { ignore: RESOLVED_MODULES }],
    'import/prefer-default-export': [OFF],
    'jsx-a11y/anchor-is-valid': [OFF],
    'jsx-a11y/click-events-have-key-events': [OFF],
    'jsx-a11y/label-has-for': [WARNING],
    'jsx-a11y/no-static-element-interactions': [OFF],
    'max-len': [ERROR, { code: 120 }],
    'no-debugger': [WARNING],
    'object-curly-newline': [OFF],
    'prefer-destructuring': [OFF],
    'react/forbid-prop-types': [WARNING],
    'react/jsx-filename-extension': [ERROR, { extensions: ['.js'] }],
    'react/no-array-index-key': [WARNING],
    'react/prop-types': [OFF],
    'react/require-default-props': [OFF],
    'yoda': [ERROR, 'always'],
    'radix': [OFF],
    'semi': [ERROR, 'never'],
    'eqeqeq': [ERROR, "always"],
    'comma-dangle': [ERROR, "always-multiline"],
    'react/jsx-sort-props': [ERROR, {
      shorthandLast: true,
      callbacksLast: true,
    }],
    'sort-keys': [ERROR, 'asc'],
    'no-multiple-empty-lines': [ERROR, { "max": 1, "maxEOF": 1 }],
    'react/sort-comp': [OFF],
  },
  'env': {
    'jest/globals': true
  },
  globals: {
    document: true,
    window: true,
  },
}
