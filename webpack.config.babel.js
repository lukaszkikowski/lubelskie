import path from 'path'
import HtmlWebpackPlugin from 'html-webpack-plugin'

const inProject = path.resolve.bind(path, `${__dirname}`);
const distName = "dist";

module.exports = {
  entry: {
    main: [
      inProject("src/main.js")
    ]
  },
  output: {
    path: inProject(`./${distName}`),
    filename: "app.[hash].js",
    publicPath: `/`
  },
  target: "web",
  module: {
    rules: [
      {
        test: /\.(js)$/,
        exclude: [ /node_modules/ ],
        use: [ "babel-loader" ]
      }
    ]
  },
  performance: {
    hints: false
  },
  watch: false,
  resolve: {
    extensions: [ "*", ".js" ],
    alias: {
      constants: inProject("src/constants"),
      pages: inProject("src/pages"),
      services: inProject("src/services"),
    }
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/index.html'
    })
  ]
};
