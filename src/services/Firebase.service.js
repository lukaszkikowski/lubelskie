export const getFile = (district, fileName) => (
  window.firebase
  .storage()
  .ref(district)
  .child(fileName)
  .getDownloadURL()
  .then(
    (data) => data, 
    () => null
  )
)
