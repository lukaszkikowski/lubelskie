import '@babel/polyfill'

import React from 'react'
import ReactDOM from 'react-dom'

import { GlobalStyle } from './styled/global'
import { HomePage } from './pages'

const App = () => (
  <React.Fragment>
    <GlobalStyle />
    <HomePage />
  </React.Fragment>
)

ReactDOM.render(<App />, document.getElementById('app'))
