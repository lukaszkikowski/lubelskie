import _ from 'lodash'

import React, { Component } from 'react'
import autoBind from 'autobind-decorator'
import CircularProgress from '@material-ui/core/CircularProgress';

import districts from 'constants/districts'
import * as FirebaseService from 'services/Firebase.service'

import Map from './components/Map'
import Sidebar from './components/Sidebar'

import { StyledSpinnerContainer } from './HomePage.styled'

@autoBind
class HomePageContainer extends Component {
  constructor(props) {
    super(props)

    this.database = window.firebase.database()

    this.state = {
      isLoaded: false,
      sidebarIsOpen: false,
      data: null,
      storage: null,
    }
  }

  componentDidMount() {
    this.loadData()
  }

  loadData() {
    const results = districts.map(async (district) => {
      return await this.database.ref(`${district}`).once('value').then((snapshot) => {
        if (!snapshot.val()) {
          return { key: district, value: null }
        }

        return { key: district, value: snapshot.val() }
      })
    })

    Promise.all(results).then((response) => {
      const data = {}
      _.each(response, ({ value, key }) => {
        if (value) {
          data[key] = value
        }
      })

      this.setState({
        isLoaded: true,
        data,
      })
    })
  }

  handleClose() {
    this.setState({
      sidebarIsOpen: false,
      storage: null,
    })
  }

  async showSidebar(e) {
    const { target: { id } } = e
    
    const data = this.state.data[id]

    if (!data) {
      return alert(id)
    }

    const starosta = await FirebaseService.getFile(id, 'starosta.jpg')
    const vogt = await FirebaseService.getFile(id, 'vogt.jpg')
    const president = await FirebaseService.getFile(id, 'president.jpg')
    const mayor = await FirebaseService.getFile(id, 'mayor.jpg')
    
    this.setState({
      sidebarIsOpen: true,
      currentElement: data,
      storage: {
        starosta, 
        vogt, 
        president, 
        mayor
      }
    })
  }

  render() {
    const {
      isLoaded,
      sidebarIsOpen,
      data,
      currentElement,
      storage,
    } = this.state

    if (!isLoaded) {
      return (
        <StyledSpinnerContainer>
          <CircularProgress />
        </StyledSpinnerContainer>
      )
    }

    return (
      <React.Fragment>
        <Sidebar
          currentElement={currentElement}
          open={sidebarIsOpen}
          storage={storage}
          onClose={this.handleClose}
        />
        <Map
          data={data}
          showSidebar={this.showSidebar}
        />
      </React.Fragment>
    )
  }
}

export default HomePageContainer
