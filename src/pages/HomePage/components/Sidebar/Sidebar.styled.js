import styled from 'styled-components'

export const StyledContainer = styled.div`
  padding: 20px;
  width: 340px;
  font-size: 18px;
`

export const StyledTitle = styled.h2`
  font-size: 24px;
  font-weight: 800;
  margin: 0;
  margin-bottom: 15px; 
`

export const StyledP = styled.p`
  margin: 0;
  margin-bottom: 10px;  
`

export const StyledImage = styled.div`
  border: 2px solid #95a5a6;
  width: 240px;
  height: 320px;
  margin-bottom: 20px;
  background-size: cover;
  background-position: center center;
  background-repeat: no-repeat;
  ${({ img }) => img && `
    background-image: url(${img});
  `}
`
