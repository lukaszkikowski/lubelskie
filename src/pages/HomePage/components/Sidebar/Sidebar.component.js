import React from 'react'

import Drawer from '@material-ui/core/Drawer'

import {
  StyledContainer,
  StyledP,
  StyledImage,
  StyledTitle,
} from './Sidebar.styled'

const Sidebar = ({
  currentElement,
  open,
  storage,
  onClose,
}) => {
  if (!currentElement) {
    return null
  }

  const {
    gasworks,
    gasification,
    president,
    mayor,
    starosta,
    title,
    vogt,
    powiat,
  } = currentElement

  return (
    <Drawer
      anchor="right"
      open={open}
      onClose={onClose}
    >
      <StyledContainer>
        <StyledTitle>
          {title}
        </StyledTitle>
        {powiat && (
          <StyledP>
            <strong>Powiat:</strong>
            {' '}
            {powiat}
          </StyledP>
        )}
        {gasworks && (
          <StyledP>
            <strong>Gazownia:</strong>
            {' '}
            {gasworks}
          </StyledP>
        )}
        {gasification && (
          <StyledP>
            <strong>Stopień gazyfikacji w %:</strong>
            {' '}
            {gasification}
          </StyledP>
        )}
        {vogt && (
          <StyledP>
            <strong>Wójt:</strong>
            {' '}
            {vogt}
          </StyledP>
        )}
        {storage && storage.vogt && (
          <StyledImage
            img={storage.vogt}
          />
        )}
        {president && (
          <StyledP>
            <strong>Prezydent:</strong>
            {' '}
            {president}
          </StyledP>
        )}
        {storage && storage.president && (
          <StyledImage
            img={storage.president}
          />
        )}
        {mayor && (
          <StyledP>
            <strong>Burmistrz:</strong>
            {' '}
            {mayor}
          </StyledP>
        )}
        {storage && storage.mayor && (
          <StyledImage
            img={storage.mayor}
          />
        )}
        {starosta && (
          <StyledP>
            <strong>Starosta:</strong>
            {' '}
            {starosta}
          </StyledP>
        )}
        {storage && storage.starosta && (
          <StyledImage
            img={storage.starosta}
          />
        )}
      </StyledContainer>
    </Drawer>
  )
}

export default Sidebar
