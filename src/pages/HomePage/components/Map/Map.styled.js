import styled from 'styled-components'

export const StyledMap = styled.div`
  path:not(.frame) {
    cursor: pointer;
    transition: .2s;
    fill: #FFF;
    
    &:hover {
      fill: #27ae60;
    }
  }
`

export const StyledText = styled.text`
  font-size: 16px;
  font-family: 'Nunito', sans-serif;
  pointer-events: none;
`