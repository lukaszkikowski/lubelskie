import { createGlobalStyle } from 'styled-components'

export const GlobalStyle = createGlobalStyle`
  body {
    font-family: 'Nunito', sans-serif;
    background: #bdc3c7;
    padding: 20px;
  }
`